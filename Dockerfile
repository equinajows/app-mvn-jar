FROM openjdk:11.0.6-jdk
WORKDIR /workspace
COPY /target/app*.jar app.jar
EXPOSE 9090
ENTRYPOINT exec java -jar /workspace/app.jar
