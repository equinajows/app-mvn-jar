package com.prueba.equinajo.appspringboot;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Date;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class AppSpringBootApplicationTests {

	@Test
	void contextLoads() {
	}
	
	@Test
	void crearUsuarioTest() {
		Producto producto = new Producto();
		producto.setNombre("Lapiz");
		producto.setCategoria("ALTA");
		producto.setDescripcion("Para cuaderno");
		producto.setDescripcionCliente("Edson");
		producto.setFechaVencimiento(new Date());
		producto.setFragil(true);
		producto.setTipo("Escolar");
		producto.setIdProducto(new Long(1));
		Producto pRetorno = producto;
		
		assertTrue(pRetorno.getIdProducto().equals(producto.getIdProducto()));
	}

}
