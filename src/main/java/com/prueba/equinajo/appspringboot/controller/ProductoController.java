package com.prueba.equinajo.appspringboot.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by equinajo on 10/01/2021.
 */

@RestController
@RequestMapping("/producto")
public class ProductoController {

    @RequestMapping(method = RequestMethod.GET)
    public String mostrar(){
        return "Sistema de Productos";
    }
}
